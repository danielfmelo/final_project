package utils;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtils {

    //private bstatic final SessionFactory sessionFactory = buildSessionFactory();
    private static final EntityManagerFactory entityManagerFactory = buildEntityManagerFactory();

    private static EntityManagerFactory buildEntityManagerFactory() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        return entityManagerFactory;
    }

    public static EntityManagerFactory getEntityManager() {
        return entityManagerFactory;
    }

    public static void shutdown() {
        getEntityManager().close();
    }

}