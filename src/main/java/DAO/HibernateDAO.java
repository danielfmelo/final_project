package DAO;
import model.User;
import utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class HibernateDAO {
    EntityManagerFactory factory = null;
    EntityManager entityManager = null;

    private static HibernateDAO single_instance = null;

    private HibernateDAO()
    {
        factory = HibernateUtils.getEntityManager();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static HibernateDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new HibernateDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<User> getBanks() {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from User";
            List<User> cs = (List<User>)entityManager.createQuery(sql).getResultList();
            entityManager.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }
    }

    public User login(String login, String pass) {
        User user = null;

        entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        String sql = "from User where name=" + login;
        user = (User)entityManager.createQuery(sql).getSingleResult();
        entityManager.getTransaction().commit();

        return user;
    }



    /** Used to get a single customer from database */
    public User getCustomer(int id) {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Bank where id=" + Integer.toString(id);
            User c = (User)entityManager.createQuery(sql).getSingleResult();
            entityManager.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }
    }

    public boolean addBank(User bank) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean removeBank(int id) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            User bank = entityManager.find(User.class, id);
            entityManager.remove(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updateBank(int id, String name) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            User bank = entityManager.find(User.class, id);
            bank.setName(name);
            entityManager.merge(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }
}
